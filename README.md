# Edu, a Hugo theme

**Edu** is Hugo theme for making educational websites!

 More information coming soon!

## How to use

### Add as a submodule

In your site directory:
```git
git submodule add git@gitlab.com:jwtay/hugo-theme-edu.git themes/edu
```

In your site's config.toml, add the line:
```toml
theme = "edu"
```

## Support

Edu is very new and there may be bugs. If you encounter any, please [report them](https://gitlab.com/jwtay/hugo-theme-edu/-/issues).

## Roadmap

### Courses
Courses will be the main content type in Edu. The idea behind this content is to enable educators to easily create static courses, similar to online course platforms like Canvas, edX, Coursera, and Udacity.

- [ ] Basic course page structure
- [ ] Exercises
- [ ] Quizzes

## Contributing

Since this project is so new, I am currently not accepting contributions. Check back later.

## Authors

* Jian Wei Tay

## License

This project is open source under the GNU Afero General Public License V3 (GNU AGPLv3). This (very briefly) means that you may copy, distribute, and modify the software, as long as your code is made open source as well. See the [full license](https://gitlab.com/jwtay/hugo-theme-edu/-/blob/master/LICENSE) text for details.
