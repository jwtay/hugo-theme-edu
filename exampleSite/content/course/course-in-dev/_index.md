---
title: "Course in development"
date: "2021-03-06"
description: "This course has no pages"
draft: false
courseid: "course-in-dev"
weight: 2
toc: true
featured: true

categories: ["example"]
tags: ["git", "python"]
course: ["example"]
pathways: ["Getting started"]
---

This is an example of a new course that is still in development (i.e. it has no other pages.)