---
title: "Course with YouTube"
date: "2021-03-06"
description: "This course has no featured image and has a YouTube video"
draft: false
courseid: "course-with-youtube"
weight: 2
toc: true
featured: true

categories: ["example"]
tags: ["markdown"]
pathways: ["Getting started"]
---

