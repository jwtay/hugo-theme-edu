---
title: "Example course"
date: "2021-03-06"
description: "Example of an Edu course"
draft: false
courseID: "course-example"
weight: -1
featured: true
tags: ["git"]
---

## Courses

At the heart of Edu's content are courses, which are a collection of pages belonging to a single subject. You can see an example of a course right here in Edu's getting started guide.

Currently, courses can only comprise of text but we are planning to integrate YouTube video functionality shortly. In the long term, we hope to implement features such as quizzes and integrations with other online platforms.