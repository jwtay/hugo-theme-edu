---
title: "Adding Edu to Hugo"
date: "2021-03-06"
description: "Learn how to add the Edu theme to Hugo"
draft: false
courseID: "course-example"
---
   
## As a Git submodule

The easiest way to include Edu in your Hugo site is to include it as a submodule.

1. Navigate to the folder containing your new site:
    ```bash
    cd my-new-site
    ```

2. Add Edu as a submodule:
    ```bash
    git submodule add git@gitlab.com:jwtay/hugo-theme-edu.git themes/edu
    ```

The Edu code should have been created under the directory `my-new-site/themes/edu`

3. Select Edu as your hugo theme: Edit the file `my-new-site/config.toml`
    ```toml
    theme = "edu"
    ```
