---
title: "Post with featured image"
date: "2021-03-09"
description: "News posts"
draft: false
featimgcredit: "Image credit: [Test](#)"
tags: ["hello", "hi"]

---

The easiest way to create a post with a featured image is to create a folder under the `/posts/` directory. Name the folder something unique so you can find this again easily. Then, place the featured image, named "featured.*" in the same directory. Edu will automatically detect this file and set it as the featured image.

{{< image "featured.jpg" "Image credit: This is **great**" "Confetti falling on the ground" >}}

{{< image src="featured.jpg" caption="Image credit: This is **great**" alt="Confetti falling on the ground" >}}

{{< image "featured.jpg" >}}