---
title: "Edu (Hugo theme)"
date: "2021-03-06"
description: "Edu is a Hugo theme for making educational websites"
draft: false
---

{{< heroimage img="featured.jpg" btn1_href="#" btn1_text="Button 1" btn2_href="#" btn2_text="Button 2">}}
# Welcome to Edu
_Let's do something!_
{{< /heroimage >}}

{{< homemain >}}

Edu is a Hugo theme for educational websites. It is built to be easy to use, allowing educators to focus on developing teaching material rather than designing a website. 

Edu was inspired by online learning platforms, such as Canvas and edX, which allow educational materials to be available to everyone via the internet.

{{< /homemain >}}

<div class="container mt-4">

  {{< featuredcourses >}}

</div>